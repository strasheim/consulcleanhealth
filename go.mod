module consulcleanhealth

go 1.18

require (
	github.com/peterbourgon/diskv v2.0.1+incompatible
	github.com/quipo/statsd v0.0.0-20180118161217-3d6a5565f314
	github.com/shirou/gopsutil v3.21.11+incompatible
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/google/btree v1.1.2 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.4.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/sys v0.0.0-20220727055044-e65921a090b8 // indirect
)
