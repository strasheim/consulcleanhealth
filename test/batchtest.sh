#!/bin/bash +ex

cp ../consulcleanhealth stablehealth
cp ../consulcleanhealth purestats
cp ../consulcleanhealth consulcleanhealth

echo "::: Getting current uptime"
uptime -p

echo "::: Checking 2 error default of stablehealth"
if [[ -e /tmp/cab95224e9bfdec8863bb6bb707ce6c6 ]] ;then  
    rm /tmp/cab95224e9bfdec8863bb6bb707ce6c6
fi

echo "::: Stablehealth Gracetime check"
GT=900 ./stablehealth ./fail.sh | grep "Gracetime!"
if [[ $? != 0 ]] ;then
   echo "Gracetime does not work"
   exit 2 
fi

echo "::: StableHealth failcount check"
GT=0 ./stablehealth ./fail.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "First fail caused an alert"
    exit 2 
fi

GT=0 ./stablehealth ./fail.sh  >/dev/null
if [[ $? != 2 ]] ;then 
    echo "2nd fail didn't trigger critical alert"
    exit 2
fi

echo "::: Stablehealth switched worked:"
echo "::: Checking ENV overwrite of failcount"

FC=4 GT=0 ./stablehealth ./fail.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "FC set to 4 this is just the 3rd fail"
    exit 2          
fi

FC=4 GT=0 ./stablehealth ./fail.sh >/dev/null
if [[ $? != 2 ]] ;then
    echo "4Th fail didn't trigger critical alert"
    exit 2
fi  

echo "::: Checking error code change of EC flag"
EC=6 GT=0 ./stablehealth ./failcode.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "Errorcode correction failed"
    exit 2
fi

echo "::: Checking error code ignoring IE flag"
IE=6 GT=0 ./stablehealth ./failcode.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "Errorcode correction failed"
    exit 2
fi

GT=2 ./stablehealth ./failcode.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "Errorcode correction failed"
    exit 2
fi

echo "::: Checking pure stats ignoring failures"
./purestats ./fail.sh >/dev/null
if [[ $? != 0 ]] ;then
    echo "Error still reported"
    exit 2
fi


