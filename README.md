### Consul Clean Health Output

Monitoring in consul itself isn't a good place to collect metrics. Very simple checks like pgrep or nagios probes create noise with the changing output they produce. Each time consul has a new "String" it will tell that everyone in the cluster and update the health status. If that is not welcome, like it is in the case of [consul2pd] (https://gitlab.com/strasheim/consul2pd) this wrapper around the check will silence successes. 

It will further add simple statsD information around those checks. This allows collectD level monitoring to be piggy bagged on the consul health checks. 



#### Cleaning, Mangling and Defaults
* Cleaning 
   * Error codes will be reduced to 0 and 2 
   * 'Success' is the only output on success, without informational noise
   * Only log to syslog on success if output is present, always print on failure 
* Mangling
   * A graceperiod of in seconds, during that time no failure is reported  
   * A FailureCount that is required before a failure is reported 
   * Subcommand output is logged to syslog if syslog is available 
   * Changing an Error code if a error code is expected 
   * Ignoring an Error code if the error code should not change the fail count 
   * On success new lines logged to syslog are removed and displayed by \n 
* Defaults
   * Internal errors encountered will result in exit status 2, ignoring graceperiod and failurecount
   * StatsD info is submission on each execution  
   * If run as *stablehealth* -> graceperiod = 180 and failurecount = 2 
   * If run as *purestats*    -> No subcommand failures reported to consul. StatsD counters report as normal


#### Options 
Since it's planed to have this run as wrapper all options are passed in as ENV variables. 

* SN | ServiceName = set an alternative name for statsD 
* GT | GraceTime = set the number of seconds since boot during which no execution of the subcommand is done  
* FC | FailureCount = set the number of failures that need to be seen in sequence before probe returns failure
* EC | ErrorCorrection = set the error code to 0 for a single given error code 
* IE | IgnoreError = the error count isn't changed if that exit code is seen. EC is happening brefore IE.
* PN | PreserveNewlines = on success normally new lines are removed from the syslog stream, if required use PN.

#### StatsD details
The CCH will submit to local 8125 UDP stats information. The information is very basic.
 
* On success Gauge 0 is send   
* During Gracetime Gauge 1 is send 
* On Failure Gauge 2 is send
* Naming convention consulcheck.*subcommand*.status
* FailureCount does not change StatsD information
* Internal errors to the consulcleanhealth will very likely not emit any statsD information.
* If no local statsD is present, the UDP package is still pushed out, but no error is raised
* In the path symblos are replaced: /@:! -> _ and . just removed. 
* If the SN variable is set, there is no replacement to those. Allowing you to add the logic you might need. 
* IE flag will result in a Gauge 1 if the error code is present

----
#### Examples

Setting BetterName as for statsD 

```
{
  "check": {
    "name": "stable-check",
    "script": "SC=BetterName /usr/local/bin/consulcleanhealth echo Hallo Welt",
    "interval": "10s"
  }
}
```

Without SC set, the subcommand is used: echo in this case

```
U 127.0.0.1:xxxxx -> 127.0.0.1:8125
consulcheck.echo.status:0|g
```

With SC set to BetterName

```
U 127.0.0.1:xxxxx -> 127.0.0.1:8125
consulcheck.BetterName.status:0|g
```

Set a failure count to 3, therefore 3 consecutive errors are needed to trigger an error to consul. StatsD does get correct details for each run. 

```                                                                     
{                                                                        
  "check": {                                            
    "name": "NeededForOthers",                             
    "script": "FC=3 /usr/local/bin/consulcleanhealth pgrep -f importantserverprocess",
    "interval": "10s"                                                                                
  }                                                                                                  
}                                                                                                                
```
   
300 Seconds of grace time, during that time the pgrep isn't run. StatsD data will show a 1 during that time. 

```                                                                     
{
  "check": {
    "name": "NeededForOthers",
    "script": "GT=300 /usr/local/bin/consulcleanhealth pgrep -f serverprocess",
    "interval": "10s"
  }
}
```   

Combining example 1 and 2 

```
{
  "check": {
    "name": "stable-check",
    "script": "FC=3 SC=BetterName /usr/local/bin/consulcleanhealth echo Hallo Welt",
    "interval": "10s"
  }
}
```

Nagios Unknown (3) output code converted to 0 

```                  
{                    
  "check": {         
    "name": "nagios_ntp_check",
    "notes": "https://serverfault.com/questions/625027/nagios-check-ntp-time-offset-unknown",
    "script": "EC=3 consulcleanhealth check_ntp_time -q -H 0.amazon.pool.ntp.org -w 10 -c20 ",
    "interval": "10s",
    "status": "passing"
  }                  
}                    
```     


### Building 

If go 1.7 is installed and go ENV paths are set in regular fashion. 

```
go get -u gitlab.com/strasheim/consulcleanhealth
```

Make sure you create hardlinks or copies of the binary as well.

```
ln consulcleanhealth stablehealth
ln consulcleanhealth purestats
```

### Download linux binary

Since it's a go binary you can go directly to stable binaries from here. Gitlab has a nice CI feature.
It's build only for x64 linux, if you need other versions, you have to build it yourself.

```
wget https://gitlab.com/strasheim/consulcleanhealth/-/jobs/artifacts/master/download?job=consulcleanhealth
```


### Mind the shell, yet it's your friend 
Consul checks are executed in a shell context. The consulcleanhealth does not change that. A couple of examples:

* "script": "consulcleanhealth /usr/local/sbin/check_dns -H $(echo www.google.com) -w 1 -t 2"		
* "script": "consulcleanhealth /usr/local/sbin/check_dns -H \`dig +short www.google.com\` -w 1 -t 2"
* "script": "consulcleanhealth /usr/local/sbin/check_dns -H www.google.com -w 1 -t 2 | grep time"
* "script": "consulcleanhealth /usr/local/sbin/check_dns -H unknown.unknown -w 1 -t 2 || exit 0"

1 and 2 will work as the backsticks and the $() are run before the results are passed as arguments to consulcleanhealth. 
3 and 4 will compile but might not produce what you are looking for. Argument to consulcleanhealth is only everything up to the pipe. Since it's shell it will interpret the second part as it's own command. Pipes and logical operator will be handled outside of consulcleanhealth.

Consul in version 1.+ allows for just using an arguement array and to not use a shell. Script is marked depricated but without version where it will be removed. There are 2 options with the **args** to still fully use consulcleanhealth.

```
{
  "check": {
    "name": "consul1.0",
    "args": ["sh","-c", "FC=10 /go/bin/consulcleanhealth false"],
    "interval": "10s",
    "status": "passing"
  }
}
```
This is using the shell you want to use and runs the command as before. 
The second option works only if no ENV vars need to be set. 

```
{
  "check": {
    "name": "consul1.0",
    "args": ["consulcleanhealth","echo","No Shell was called during this execution"],
    "interval": "10s",
    "status": "passing"
  }
}
```

The later assumes that both binaries are in the "$PATH" which is used by the **args** also it's not mentioned. 
Environment variables which are present in the shell that spawned consul will be present in the args call as well. 



 

#### Initial service status
Every consul check starts in critical by default. The grace time provided here does not change that. To have a service registered as healthy you have to this in the service check.

```
{
  "check": {
    "script": "stablehealth check_mem",
    "interval": "10s",
    "status": "passing"
  }
}
```

This service will start healthy and by the defaults of the stablehealth parameters not result in critical for 180 seconds. Please be mindful with that status option. If other services are configured base on the health of a service, this can cause downtime. In this example, checking memory or diskspace or abusing consul as a cron job it's very likely what you are looking for. 


#### Consul check timeout
If the check you run takes longer than the timeout the error code of 2 is returned by consul itself. It will not wait for the probe to submit it's result, as such consulcleanhealth results will be ignores. This is important to note if you use stablehealth to overwrite single errors. While the documentations of consul isn't clear on that, you set a higher timeout than interval. 



#### Rare usage
Consulcleanhealth will read from stdio. Meaning you can pipe into it. This allows for a few alternative use cases. For clearity I would generally advise to not do that. It should be nearly in all cases make more sense, if the script line is getting long and contains a few logical operators to create a script and call that. This allows for much simpler debugging and the script allows for comments. That said, the use cases are there. 

```
{
  "check": {
    "name": "testing",
    "script": "echo 'Funny things to print'| FC=3 /Users/mark/bin/consulcleanhealth cat ",
    "interval": "5s"
  }
}
```

Here the SN flag becomes more useful. Since you might have a few greps out there, it would flatten the statsD a bit too much. 
Depending on your monitoring needs, you can also set SN to something like *SN=grep.funny*. The dot would make it to the backend.

```
{
  "check": {
    "name": "greptest",
    "script": "echo 'Funny things to print'| SN=FunnyPrint /Users/mark/bin/consulcleanhealth grep print",
    "interval": "5s"
  }
}
```

#### Vendoring 
No vendoring was done as part of this build. The build works with 3 libaries.

* github.com/peterbourgon/diskv - MIT License
* github.com/quipo/statsd - MIT License
* github.com/shirou/gopsutil/host - BSD 3-clause "New" or "Revised" License

#### EcoSystem 
CCH is part of the whole system of small tools to enhance [consul](https://www.consul.io). 

* [consul2pd](https://gitlab.com/strasheim/consul2pd) links consul monitoring events to Pagerduty 
* [consul4ASG](https://gitlab.com/strasheim/consul4asg) links consul monitoring events to AWS autoscaling group events
* [consulwatches](https://gitlab.com/strasheim/consulwatches) autogenerates monitoring by consul TAGs 
* [ConsulCleanHealth](https://gitlab.com/strasheim/consulcleanhealth) enhances monitoring options from Consul


