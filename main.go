// The MIT License (MIT)
//
// Copyright (c) 2017 Mark Strasheim
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Package cleanconsulhealth should make consul monitoring more useful
// It add defaults and make the output more consistent
// StatsD and Syslog integration is added to all consul checks
package main

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/peterbourgon/diskv"
	"github.com/quipo/statsd"
	"github.com/shirou/gopsutil/host"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("No binary to execute supplied")
		os.Exit(2)
	}

	nameParts := strings.Split(os.Args[0], "/")
	myName := nameParts[len(nameParts)-1]

	var gracetime, checkname, failcount string

	switch myName {
	case "stablehealth":
		gracetime = "180"
		failcount = "2"
		break
	case "purestats":
		gracetime = "0"
		failcount = "0"
		break
	}
	if len(os.Getenv("GT")) > 0 {
		gracetime = os.Getenv("GT")
	}
	if len(os.Getenv("SN")) > 0 {
		checkname = os.Getenv("SN")
	}
	if len(os.Getenv("FC")) > 0 {
		failcount = os.Getenv("FC")
	}

	logwriter, err := syslog.New(syslog.LOG_NOTICE, os.Args[1])
	if err == nil {
		log.SetFlags(0)
		log.SetOutput(logwriter)
	} else {
		log.SetFlags(0)
		log.SetOutput(ioutil.Discard)
	}

	undotted := os.Args[1]
	undotted = strings.Replace(undotted, ".", "", -1)
	undotted = strings.Replace(undotted, ":", "_", -1)
	undotted = strings.Replace(undotted, "|", "_", -1)
	undotted = strings.Replace(undotted, "@", "_", -1)
	undotted = strings.Replace(undotted, "/", "_", -1)
	prefix := "consulcheck." + undotted + "."
	if len(checkname) > 1 {
		prefix = "consulcheck." + checkname + "."
	}

	statspush := statsd.NewStatsdClient("127.0.0.1:8125", prefix)
	err = statspush.CreateSocket()
	errout(err)
	defer statspush.Close()

	uptime, err := host.Uptime()
	errout(err)
	var gtime uint64
	if len(gracetime) > 1 {
		gtime, err = strconv.ParseUint(gracetime, 10, 64)
		errout(err)
	} else {
		gtime = 0
	}

	if uptime < gtime {
		log.Println("Gracetime!")
		fmt.Println("Gracetime!")
		statspush.Gauge("status", 1)
		os.Exit(0)
	}

	db := diskv.New(diskv.Options{
		BasePath:     "/tmp/",
		Transform:    func(s string) []string { return []string{} },
		CacheSizeMax: 1024 * 1024,
	})

	var args []string

	for i, value := range os.Args {
		switch i {
		case 0:
			break
		case 1:
			break
		default:
			args = append(args, value)
		}
	}

	fcount := int64(0)
	oldfailcount := int64(0)
	if len(failcount) > 0 {
		fcount, err = strconv.ParseInt(failcount, 10, 64)
	}

	checksum := md5.Sum([]byte(strings.Join(os.Args, "_")))
	dbkey := fmt.Sprintf("%x", checksum)
	oldfailcountbytes, err := db.Read(dbkey)
	if err != nil {
		oldfailcount = 0
	} else {
		oldfailcount, _ = strconv.ParseInt(fmt.Sprintf("%s", oldfailcountbytes), 10, 64)
		errout(err)
	}

	binary, err := exec.LookPath(os.Args[1])
	errout(err)

	cmd := exec.Command(binary, args...)
	cmd.Env = os.Environ()

	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr

	file, err := os.Stdin.Stat()
	errout(err)

	stdin, err := cmd.StdinPipe()
	errout(err)

	err = cmd.Start()
	errout(err)

	// If stdin is having data, data is piped in and push forward
	if file.Mode()&os.ModeNamedPipe != 0 {
		io.Copy(stdin, os.Stdin)
		defer os.Stdin.Close()
	}
	stdin.Close()

	err = cmd.Wait()
	if (len(os.Getenv("EC")) > 0) && (err != nil) {
		if err.Error() == "exit status "+os.Getenv("EC") {
			log.Println("Changing error code to zero ")
			err = nil
		}

	}

	if (len(os.Getenv("IE")) > 0) && (err != nil) {
		if err.Error() == "exit status "+os.Getenv("IE") {
			log.Println("Check result ignored")
			fmt.Println("Ignored!")
			statspush.Gauge("status", 1)
			os.Exit(0)
		}
	}

	if err != nil {
		log.Println(fmt.Sprint(err) + ": " + stderr.String() + out.String())
		fmt.Println(fmt.Sprint(err) + ": " + chomp(stderr.String()) + chomp(out.String()))
		statspush.Gauge("status", 2)
		oldfailcount++
		db.Write(dbkey, []byte(strconv.FormatInt(oldfailcount, 10)))
		if oldfailcount < fcount {
			os.Exit(0)
		}
		if myName == "purestats" {
			os.Exit(0)
		}
		os.Exit(2)
	}

	if len(out.String()) > 0 {
		if len(os.Getenv("PN")) > 0 {
			log.Println(out.String())
		} else {
			log.Println(strings.Replace(out.String(), "\n", "\\n", -1))
		}
	}

	fmt.Println("Success!")
	statspush.Gauge("status", 0)
	db.Write(dbkey, []byte("0"))
	os.Exit(0)
}

// Exit on error. It will the error and print to terminal.
// Exit status is 2 to be in line with monitoring requirement
func errout(err error) {
	if err != nil {
		log.Println(err)
		fmt.Println(err)
		os.Exit(2)
	}
}

// Old Perl habit. Removes is present a \n at the end of string
func chomp(s string) string {
	if strings.HasSuffix(s, "\n") {
		return s[:len(s)-1]
	}
	return s
}
